import { Controller, Get, Post, Body, Render, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { LoginUsersType } from './types/types';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  index(@Res() res) {
    res.status(302).redirect('/login');
  }

  @Get('/login')
  @Render('login')
  getLogin() {
    const message = 'Hello world';
    return { message: message };
  }

  @Post('/login')
  postLogin(@Body() data) {
    this.appService.loginUser(<LoginUsersType>data);
    return {};
  }

  @Get('/register')
  @Render('register')
  getRegister() {}

  @Get('/dashboard')
  @Render('dashboard')
  getDashboard() {}

  @Get('/users-list')
  @Render('usersList')
  getUsersList() {
    const users = [
      { id: 1, name: 'Fake User', email: 'fakeuser@example.com' },
      { id: 2, name: 'Fake User 2', email: 'fakeuser2@example.com' },
    ];
    return { users };
  }
}
