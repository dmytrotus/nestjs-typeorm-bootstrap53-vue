import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './typeorm/entities/User';
import { LoginUsersType } from './types/types';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  getLogin(): string {
    return 'Login Page!';
  }

  loginUser(data: LoginUsersType) {
    // if user exists in database
    // check users hashed password
    // if password correct login user
    // if not return the error
    console.log(data);
  }

  getRegister(): string {
    return 'Register Page!';
  }

  getDashboard(): string {
    return 'Dashboard Page!';
  }
}
